package com.pcomm.service.category.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcomm.service.category.dto.CategoryDTO;
import com.pcomm.service.category.entity.Category;
import com.pcomm.service.category.entity.CategoryLanguage;
import com.pcomm.service.category.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryServiceMock;

    @Autowired
    ObjectMapper objectMapper;

    private static final String url = "/category";

    CategoryDTO categoryDTO = new CategoryDTO();
    Category category = new Category();
    Category parentCategory = new Category();
    CategoryLanguage categoryLanguage = new CategoryLanguage();
    List<CategoryDTO> categories = new ArrayList<>();

    @Before
    public void setup() {
        categoryDTO.setId(1);
        categoryDTO.setCatNameTr("test Cat turkish");
        categoryDTO.setCatName("test cat");
        categoryDTO.setParentId(50);

        parentCategory.setName("test parent cat");
        parentCategory.setId(1);

        categoryLanguage.setNameTr("test Cat turkish");
        categoryLanguage.setId(1);
        category.setCategoryLanguage(categoryLanguage);
        category.setParent(parentCategory);
        category.setId(2);
        category.setName("test cat");

        categories.add(category.toCategoryDTO());
    }

    @Test
    public void testAddCategoryShouldSuccess() throws Exception {

        when(categoryServiceMock.addCategory(refEq(categoryDTO))).thenReturn(categoryDTO);
        mockMvc.perform(post(url + "/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.catName", is(categoryDTO.getCatName())))
                .andExpect(jsonPath("$.catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$.parentId", is(50)));

        verify(categoryServiceMock, times(1)).addCategory(refEq(categoryDTO));
    }

    @Test
    public void testUpdateCategoryShouldSuccess() throws Exception {
        CategoryDTO updatedCategoryDTO = categoryDTO;
        updatedCategoryDTO.setCatName("test updated cat");

        when(categoryServiceMock.updateCategory(refEq(categoryDTO))).thenReturn(updatedCategoryDTO);
        mockMvc.perform(put(url + "/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.catName", is(updatedCategoryDTO.getCatName())))
                .andExpect(jsonPath("$.catNameTr", is(updatedCategoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$.parentId", is(50)));

        verify(categoryServiceMock, times(1)).updateCategory(refEq(categoryDTO));
    }

    @Test
    public void testDeleteCategoryShouldSuccess() throws Exception {

        when(categoryServiceMock.deleteCategory(categoryDTO.getId())).thenReturn(categoryDTO);
        mockMvc.perform(delete(url + "/delete/" + categoryDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.catName", is(categoryDTO.getCatName())))
                .andExpect(jsonPath("$.catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$.parentId", is(50)));

        verify(categoryServiceMock, times(1)).deleteCategory(categoryDTO.getId());
    }

    @Test
    public void testGetParentCategoryShouldSuccess() throws Exception {

        when(categoryServiceMock.findParentCategories()).thenReturn(categories);
        mockMvc.perform(get(url + "/parentCategories")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].catName", is(category.getName())))
                .andExpect(jsonPath("$[0].catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$[0].parentId", is(1)));

        verify(categoryServiceMock, times(1)).findParentCategories();
    }

    @Test
    public void testGetSubCategoryShouldSuccess() throws Exception {

        when(categoryServiceMock.findCategoriesByParentId(1l)).thenReturn(categories);
        mockMvc.perform(get(url + "/subCategories/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].catName", is(category.getName())))
                .andExpect(jsonPath("$[0].catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$[0].parentId", is(1)));

        verify(categoryServiceMock, times(1)).findCategoriesByParentId(1l);
    }

    @Test
    public void testGetAllCategoriesShouldSuccess() throws Exception {

        when(categoryServiceMock.findAllCategories()).thenReturn(categories);
        mockMvc.perform(get(url + "/allCategories")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].catName", is(category.getName())))
                .andExpect(jsonPath("$[0].catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$[0].parentId", is(1)));

        verify(categoryServiceMock, times(1)).findAllCategories();
    }

    @Test
    public void testGetAllParentCategoriesShouldSuccess() throws Exception {

        when(categoryServiceMock.findParentCategoriesById(1l)).thenReturn(categories);
        mockMvc.perform(get(url + "/allParentCategories/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].catName", is(category.getName())))
                .andExpect(jsonPath("$[0].catNameTr", is(categoryDTO.getCatNameTr())))
                .andExpect(jsonPath("$[0].parentId", is(1)));

        verify(categoryServiceMock, times(1)).findParentCategoriesById(1l);
    }
}
