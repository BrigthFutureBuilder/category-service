package com.pcomm.service.category.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcomm.service.category.controller.CategoryController;
import com.pcomm.service.category.dto.CategoryDTO;
import com.pcomm.service.category.entity.Category;
import com.pcomm.service.category.entity.CategoryLanguage;
import com.pcomm.service.category.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class ControllerAdviceTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryServiceMock;

    @Autowired
    ObjectMapper objectMapper;

    @Mock
    CategoryResponseEntityExceptionHandler categoryResponseEntityExceptionHandler;

    @Mock
    CategoryController categoryController;

    private static final String url = "/category";

    CategoryDTO categoryDTO = new CategoryDTO();
    Category category = new Category();
    Category parentCategory = new Category();
    CategoryLanguage categoryLanguage = new CategoryLanguage();
    List<CategoryDTO> categories = new ArrayList<>();

    @Before
    public void setup() {
        categoryDTO.setId(1);
        categoryDTO.setCatNameTr("test Cat turkish");
        categoryDTO.setCatName("test cat");
        categoryDTO.setParentId(50);

        parentCategory.setName("test parent cat");
        parentCategory.setId(1);

        categoryLanguage.setNameTr("test Cat turkish");
        categoryLanguage.setId(1);
        category.setCategoryLanguage(categoryLanguage);
        category.setParent(parentCategory);
        category.setId(2);
        category.setName("test cat");

        categories.add(category.toCategoryDTO());

        mockMvc = MockMvcBuilders.standaloneSetup(categoryController)
                .setControllerAdvice(new CategoryResponseEntityExceptionHandler())
                .build();
    }

    @Test
    public void shoduldThrownNotFoundExWhenUpdateCategory() throws Exception {

        Mockito.when(categoryController.updateCategory(any(CategoryDTO.class))).thenThrow(new CategoryNotFoundException("There is no category to update. Category name: " + categoryDTO.getCatName()));

        mockMvc.perform(put(url + "/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryDTO)))
                .andExpect(status().is(404)).andReturn();

    }
    @Test
    public void shouldThrownRestExceptionHandlerErrorWhenAddCategory() throws Exception {

        Mockito.when(categoryController.addCategory(any(CategoryDTO.class))).thenThrow(new CategoryConflictException("Category Name exist"));

        mockMvc.perform(post(url + "/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryDTO)))
                .andExpect(status().is(409)).andReturn();

    }
    @Test
    public void givenEmptyRequestBodyShouldReturnBadRequestWhenAddCategory() throws Exception {

        CategoryDTO categoryDTO2 = new CategoryDTO();

        mockMvc.perform(post(url + "/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryDTO2)))
                .andExpect(status().is(400)).andReturn();

    }
}
