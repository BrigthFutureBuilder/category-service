package com.pcomm.service.category.services;

import com.pcomm.service.category.dto.CategoryDTO;
import com.pcomm.service.category.entity.Category;
import com.pcomm.service.category.exception.CategoryConflictException;
import com.pcomm.service.category.exception.CategoryNotFoundException;
import com.pcomm.service.category.repository.CategoryRepository;
import com.pcomm.service.category.service.impl.CategoryServiceImpl;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CategoryServicesTest {

    @InjectMocks
    private CategoryServiceImpl categoryServiceMock;

    @Mock
    private CategoryRepository categoryRepositoryMock;

    CategoryDTO categoryDTO = new CategoryDTO();
    Category category = new Category();
    Category parentCategory = new Category();
    List<Category> categories = new ArrayList<>();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        categoryDTO.setCatName("test cat name");
        categoryDTO.setCatNameTr("test cat turkish name");
        categoryDTO.setParentId(1l);
        category = categoryDTO.toCategory();
        parentCategory.setName("test cat name");
        parentCategory.setId(1);
        categories.add(category);
    }

    @Test
    public void shouldBeSuccessWhenAddingCategory() throws Exception {

        Category resultCat = new Category();
        resultCat.setName("test cat name");
        resultCat.setId(1);

        when(categoryRepositoryMock.findCategoryByNameOrCategoryLanguage_NameTr(categoryDTO.getCatName(), categoryDTO.getCatNameTr())).thenReturn(null);
        when(categoryRepositoryMock.save(any(Category.class))).thenReturn(resultCat);
        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(category));

        CategoryDTO savedCategoryDTO = categoryServiceMock.addCategory(categoryDTO);

        assertThat(savedCategoryDTO.getCatName(), Matchers.is(Matchers.equalTo(categoryDTO.getCatName())));
    }

    @Test(expected = CategoryConflictException.class)
    public void shouldThrownCatNameConflictExWhenAddingCategory() throws Exception {

        when(categoryRepositoryMock.findCategoryByNameOrCategoryLanguage_NameTr(categoryDTO.getCatName(), categoryDTO.getCatNameTr())).thenReturn(category);

        CategoryDTO savedCategoryDTO = categoryServiceMock.addCategory(categoryDTO);
    }

    @Test
    public void shouldBeSuccessWhenDeleteCategory() throws Exception {

        Category resultCat = new Category();
        resultCat.setName("test cat name");
        resultCat.setId(1);

        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(resultCat));

        CategoryDTO deletedCategoryDTO = categoryServiceMock.deleteCategory(1l);

        assertThat(deletedCategoryDTO.getId(), Matchers.is(1l));
    }

    @Test(expected = CategoryNotFoundException.class)
    public void shouldThrownCatNameNotFoundExWhenDeleteCategory() throws Exception {

        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.empty());

        categoryServiceMock.deleteCategory(1l);
    }

    @Test
    public void shouldBeSuccessWhenUpdateCategory() throws Exception {

        Category resultCat = new Category();
        resultCat.setName("test cat name updated");
        resultCat.setId(1);

        when(categoryRepositoryMock.save(any(Category.class))).thenReturn(resultCat);
        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(category));

        CategoryDTO updatedCategoryDTO = categoryServiceMock.updateCategory(categoryDTO);

        assertThat(updatedCategoryDTO.getCatName(), Matchers.is(Matchers.equalTo("test cat name updated")));
    }

    @Test(expected = CategoryNotFoundException.class)
    public void shouldThrownCatNameNotFoundExWhenUpdateCategory() throws Exception {

        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.empty());

        categoryServiceMock.updateCategory(categoryDTO);
    }

    @Test
    public void givenParentIdShouldReturnCategories() throws Exception {

        when(categoryRepositoryMock.findCategoriesByParentId(1)).thenReturn(categories);
        categoryServiceMock.findCategoriesByParentId(1l);
        assertThat(categories.size(), Matchers.is(1));
    }

    @Test
    public void shouldReturnParentCategories() throws Exception {

        when(categoryRepositoryMock.findCategoriesByParentIsNull()).thenReturn(categories);
        List<CategoryDTO> categoryDTOS = categoryServiceMock.findParentCategories();
        assertThat(categories.size(), Matchers.is(categoryDTOS.size()));
    }

    @Test
    public void shouldReturnAllCategories() throws Exception {

        when(categoryRepositoryMock.findAll()).thenReturn(categories);
        List<CategoryDTO> categoryDTOS = categoryServiceMock.findAllCategories();
        assertThat(categories.size(), Matchers.is(categoryDTOS.size()));
    }

    @Test
    public void givenCategoryIdShouldReturnParentCategories() throws Exception {
        category.setParent(parentCategory);
        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(category));
        when(categoryRepositoryMock.findCategoriesByParentId(1)).thenReturn(categories);
        List<CategoryDTO> categoryDTOS = categoryServiceMock.findParentCategoriesById(1);
        assertThat(categories.size(), Matchers.is(categoryDTOS.size()));
    }

    @Test
    public void givenCategoryIdAndWhenParentIsNullShouldReturnParentCategories() throws Exception {
        when(categoryRepositoryMock.findById(any(Long.class))).thenReturn(Optional.of(category));
        when(categoryRepositoryMock.findCategoriesByParentIsNull()).thenReturn(categories);
        List<CategoryDTO> categoryDTOS = categoryServiceMock.findParentCategoriesById(1);
        assertThat(categories.size(), Matchers.is(categoryDTOS.size()));
    }
    @Test
    public void shouldThrownJPAExWhenAddingCategory() throws Exception {
        when(categoryRepositoryMock.findCategoryByNameOrCategoryLanguage_NameTr(categoryDTO.getCatName(), categoryDTO.getCatNameTr())).thenThrow(RuntimeException.class);
        assertThatThrownBy(() -> categoryServiceMock.addCategory(categoryDTO))
                .isInstanceOf(RuntimeException.class);
    }
}