package com.pcomm.service.category.service;

import com.pcomm.service.category.dto.CategoryDTO;

import java.util.List;

public interface CategoryService {

    CategoryDTO addCategory(CategoryDTO categoryDTO);

    CategoryDTO deleteCategory(long categoryId);

    CategoryDTO updateCategory(CategoryDTO categoryDTO);

    List<CategoryDTO> findCategoriesByParentId(Long parentId);

    List<CategoryDTO> findParentCategories();

    List<CategoryDTO> findAllCategories();

    List<CategoryDTO> findParentCategoriesById(long categoryId);

}
