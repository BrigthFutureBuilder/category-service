package com.pcomm.service.category.service.impl;

import com.google.common.collect.Lists;
import com.pcomm.service.category.dto.CategoryDTO;
import com.pcomm.service.category.entity.Category;
import com.pcomm.service.category.entity.CategoryLanguage;
import com.pcomm.service.category.exception.CategoryConflictException;
import com.pcomm.service.category.exception.CategoryNotFoundException;
import com.pcomm.service.category.repository.CategoryRepository;
import com.pcomm.service.category.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public CategoryDTO addCategory(CategoryDTO categoryDTO) {
        if (!checkCategoryName(categoryDTO)){
            logger.debug("Category name exist. {}", categoryDTO.getCatName());
            throw new CategoryConflictException("Category Name exist");
        }
        Category category = categoryDTO.toCategory();
        Optional<Category> parent = this.categoryRepository.findById(categoryDTO.getParentId());
        if(parent.isPresent()){
            category.setParent(parent.get());
        }
        if(categoryDTO.getCatNameTr() != null){
            CategoryLanguage categoryLanguage = new CategoryLanguage();
            categoryLanguage.setNameTr(categoryDTO.getCatNameTr());
            categoryLanguage.setCategory(category);
            category.setCategoryLanguage(categoryLanguage);
        }
        Category savedCategory = categoryRepository.save(category);
        return savedCategory.toCategoryDTO();
    }

    @Override
    public CategoryDTO deleteCategory(long categoryId) {
        Optional<Category> category = this.categoryRepository.findById(categoryId);
        if(!category.isPresent()){
            throw new CategoryNotFoundException("There is no category to delete. Category id: " + categoryId);
        }
        categoryRepository.delete(category.get());
        return category.get().toCategoryDTO();
    }

    @Override
    public CategoryDTO updateCategory(CategoryDTO categoryDTO) {
        Category category = checkCategory(categoryDTO).get();
        category.setName(categoryDTO.getCatName());

        Optional<Category> parent = this.categoryRepository.findById(categoryDTO.getParentId());
        if(parent.isPresent()){
            category.setParent(parent.get());
        }
        Category updatedCategory = this.categoryRepository.save(category);
        return updatedCategory.toCategoryDTO();
    }

    @Override
    public List<CategoryDTO> findCategoriesByParentId(Long parentId) {
        List<Category> subCategories = this.categoryRepository.findCategoriesByParentId(parentId);
        return subCategories.stream().map(Category::toCategoryDTO).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> findParentCategories() {
        List<Category> parentCategories = this.categoryRepository.findCategoriesByParentIsNull();
        return parentCategories.stream().map(Category::toCategoryDTO).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> findAllCategories() {
        List<Category> categories = Lists.newArrayList(this.categoryRepository.findAll());
        return categories.stream().map(Category::toCategoryDTO).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> findParentCategoriesById(long categoryId) {
        List<Category> categories = new ArrayList<>();
        Optional<Category> category = this.categoryRepository.findById(categoryId);
        if(category.isPresent()){
            if(category.get().getParent() == null){
                categories = this.categoryRepository.findCategoriesByParentIsNull();
            }else{
                categories = this.categoryRepository.findCategoriesByParentId(category.get().getParent().getId());
            }
        }
        return categories.stream().map(Category::toCategoryDTO).collect(Collectors.toList());
    }

    private boolean checkCategoryName(CategoryDTO categoryDTO){
        Category category = new Category();
        try{
            category = this.categoryRepository.findCategoryByNameOrCategoryLanguage_NameTr(categoryDTO.getCatName(), categoryDTO.getCatNameTr());
        }catch (RuntimeException e){
            logger.error("JPA exception occues: {}", e.getMessage());
            return false;
        }
        if (category == null){
            return true;
        }
        return false;
    }

    /**
     * Check category id exist
     * @param categoryDTO category
     * @return found category or thrown exception
     */
    private Optional<Category> checkCategory(CategoryDTO categoryDTO){
        Optional<Category> category = this.categoryRepository.findById(categoryDTO.getId());
        if (!category.isPresent()){
            throw new CategoryNotFoundException("There is no category to delete. Category name: " + categoryDTO.getCatName());
        }
        return category;
    }
}
