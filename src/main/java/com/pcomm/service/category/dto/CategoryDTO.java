package com.pcomm.service.category.dto;

import com.pcomm.service.category.entity.Category;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CategoryDTO {

    private long id;

    @NotNull
    private String catName;

    private String catNameTr;

    private long parentId;

    public Category toCategory(){
        Category category = new Category();
        category.setName(getCatName());
        category.setId(getId());
        return category;
    }
}
