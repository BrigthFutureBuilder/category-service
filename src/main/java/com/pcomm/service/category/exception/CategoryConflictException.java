package com.pcomm.service.category.exception;

public class CategoryConflictException extends RuntimeException {

    private static final long serialVersionUID = -7131266248952296430L;

    public CategoryConflictException(String exception) {
        super(exception);
    }
}
