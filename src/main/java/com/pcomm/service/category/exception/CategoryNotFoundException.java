package com.pcomm.service.category.exception;

public class CategoryNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -7131266248952296430L;

    public CategoryNotFoundException(String exception) {
        super(exception);
    }
}
