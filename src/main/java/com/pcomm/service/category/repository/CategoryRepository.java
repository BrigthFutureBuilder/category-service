package com.pcomm.service.category.repository;

import com.pcomm.service.category.entity.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findCategoryByNameOrCategoryLanguage_NameTr(String name, String nameTr);

//    Category findCategoryByCategoryLanguage_NameTr(String nameTr);

    List<Category> findCategoriesByParentId(long parentId);

    List<Category> findCategoriesByParentIsNull();

    List<Category> findCategoriesBySubCategoriesIsIn(Category category);

}

