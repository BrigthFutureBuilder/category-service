package com.pcomm.service.category.entity;

import com.pcomm.service.category.dto.CategoryDTO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "CATEGORY")
@Getter
@Setter
public class Category {

    @Id
    @SequenceGenerator(name = "catSeqGen", sequenceName = "catSeq", initialValue = 50, allocationSize = 1)
    @GeneratedValue(generator = "catSeqGen")
    private long id;

    @Column(name = "CAT_NAME")
    @NotNull
    private String name;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Category> subCategories = new LinkedList<>();

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID",insertable=true,updatable=true)
    private Category parent;

    @OneToOne(mappedBy = "category", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private CategoryLanguage categoryLanguage;

    public CategoryDTO toCategoryDTO(){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setCatName(getName());
        categoryDTO.setId(getId());
        if(parent != null){
            categoryDTO.setParentId(parent.getId());
        }
        if(categoryLanguage != null){
            categoryDTO.setCatNameTr(categoryLanguage.getNameTr());
        }
        return categoryDTO;
    }
}
