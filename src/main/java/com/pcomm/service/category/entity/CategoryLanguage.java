package com.pcomm.service.category.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "CATEGORY_LANGUAGE")
@Getter
@Setter
public class CategoryLanguage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "CAT_NAME_TR")
    private String nameTr;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "cat_id",nullable = false)
    private Category category;

}
