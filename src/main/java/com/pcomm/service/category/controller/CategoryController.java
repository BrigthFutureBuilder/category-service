package com.pcomm.service.category.controller;

import com.pcomm.service.category.dto.CategoryDTO;
import com.pcomm.service.category.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/add")
    public ResponseEntity<CategoryDTO> addCategory(@Valid @RequestBody CategoryDTO categoryDTO){
        logger.info("Add Category started at: {}", LocalDate.now());
        CategoryDTO savedCategoryDTO = this.categoryService.addCategory(categoryDTO);
        logger.info("Category is created at: {}", LocalDate.now());
        return new ResponseEntity<>(savedCategoryDTO, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<CategoryDTO> updateCategory(@Valid @RequestBody CategoryDTO categoryDTO){
        logger.info("Update Category started at: {}", LocalDate.now());
        CategoryDTO savedCategoryDTO = this.categoryService.updateCategory(categoryDTO);
        logger.info("Category is updated at: {}", LocalDate.now());
        return new ResponseEntity<>(savedCategoryDTO, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{categoryId}")
    public ResponseEntity<CategoryDTO> deleteCategory(@PathVariable("categoryId") long categoryId){
        logger.info("Delete Category started at: {}", LocalDate.now());
        CategoryDTO savedCategoryDTO = this.categoryService.deleteCategory(categoryId);
        logger.info("Category is created at: {}", LocalDate.now());
        return new ResponseEntity<>(savedCategoryDTO, HttpStatus.OK);
    }

    @GetMapping("/parentCategories")
    public ResponseEntity<List<CategoryDTO>> getParentCategories(){
        logger.info("Get Parent Categories started at: {}", LocalDate.now());
        List<CategoryDTO> findParentCategoriesDTO = this.categoryService.findParentCategories();
        logger.info("Get Parent Categories at: {}", LocalDate.now());
        return new ResponseEntity<>(findParentCategoriesDTO, HttpStatus.OK);
    }

    @GetMapping("/subCategories/{parentId}")
    public ResponseEntity<List<CategoryDTO>> getSubCategories(@PathVariable("parentId") long parentId){
        logger.info("Get Sub Categories started at: {}", LocalDate.now());
        List<CategoryDTO> findSubCategoriesDTO = this.categoryService.findCategoriesByParentId(parentId);
        logger.info("Get Sub Categories at: {}", LocalDate.now());
        return new ResponseEntity<>(findSubCategoriesDTO, HttpStatus.OK);
    }

    @GetMapping("/allCategories")
    public ResponseEntity<List<CategoryDTO>> getAllCategories(){
        logger.info("Get All Categories started at: {}", LocalDate.now());
        List<CategoryDTO> findAllCategoriesDTO = this.categoryService.findAllCategories();
        logger.info("Get All Categories at: {}", LocalDate.now());
        return new ResponseEntity<>(findAllCategoriesDTO, HttpStatus.OK);
    }

    @GetMapping("/allParentCategories/{categoryId}")
    public ResponseEntity<List<CategoryDTO>> getAllParentCategoriesById(@PathVariable("categoryId") long categoryId){
        logger.info("Get All Categories started at: {}", LocalDate.now());
        List<CategoryDTO> findAllParentCategoriesDTO = this.categoryService.findParentCategoriesById(categoryId);
        logger.info("Get All Categories at: {}", LocalDate.now());
        return new ResponseEntity<>(findAllParentCategoriesDTO, HttpStatus.OK);
    }

}
